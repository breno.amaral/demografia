#install.packages("dplyr")
#install.packages('lubridate')
#install.packages('pacman')
#install.packages('LexisPlotR')
#install.packages('ggplot2')

#install.packages("remotes")
#remotes::install_github("rfsaldanha/microdatasus")


## QUESTÃO 1-A ##


library(read.dbc)
library(microdatasus)
library(tidyverse)
library(lubridate)
library(dplyr)
library(pacman)
library(LexisPlotR)
library(ggplot2)

#pacman::p_load(LexisPlotR, ggplot2, dplyr, read.dbc, tibble, purrr, stringr)


# Baixando o df de nascimentos
df_nasc <- read.csv("C:/Users/breno/Downloads/nasc 2000-2020.csv", sep=";")

# Nascimentos de 2021
df_nasc_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DN21OPEN/DN21OPEN.dbf")

# Baixando o de 2020 para comparar e obter os dados de 2021 pela UF de interesse
df_nasc_2020 <- read.dbc("C:/Users/breno/Downloads/arqdn/DNSE2020.dbc")

# Dados filtrados pela UF
df_nasc_2021 <- df_nasc_2021 %>%
  filter(CODMUNRES %in% df_nasc_2020$CODMUNRES)

# Contandos os nascimentos de 2021
nrow(df_nasc_2021)

# Adicionando dado referente a 2021
df_nasc[nrow(df_nasc) + 1,] <- c(2021, nrow(df_nasc_2021))     
     

# Baixando o df de óbitos

df_mortes <- fetch_datasus(year_start = 2000, year_end = 2020, uf = "SE", information_system = "SIM-DO")
df_mortes <- process_sim(df_mortes)

# Deixando apenas variáveis de interesse do total
df_mortes_inf <- subset(df_mortes, select = c(DTOBITO, DTNASC))

# Óbitos de 2021
df_mortes_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DO21OPEN/DO21OPEN.dbf")

# Baixando o de 2020 para comparar e obter os dados de 2021 pela UF de interesse
df_mortes_2020 <- read.dbc("C:/Users/breno/Downloads/arqdo/DOSE2020.dbc")

# Dados filtrados pela UF
df_mortes_2021 <- df_mortes_2021 %>%
  filter(CODMUNRES %in% df_mortes_2020$CODMUNRES)

df_mortes_2021 <- process_sim(df_mortes_2021)

# Deixando apenas variáveis de interesse de 2021
df_mortes_2021 <- subset(df_mortes_2021, select = c(DTOBITO, DTNASC))

# Retira NAs
df_mortes_2021 <- na.omit(df_mortes_2021)

# Adiciona dados referentes a 2021 no DF
df_mortes_inf = rbind(df_mortes_inf, df_mortes_2021)



# Testando função difftime
a <- difftime('2019-08-20', '2012-06-20', units="days")
b <- difftime('2022-08-27', '2022-06-20', units="days")

a
a-b


# Testando recorte de string
subs<-substr('2019-08-20',start=1,stop=4)
subs


# Função que retorna a idade de óbito do indivíduo a partir de nascimento e óbito
retorna_idade <- function(data_ob, data_nc) {
  # Ajusta os parâmetros da função
  data_ob <- ymd(data_ob)
  data_nc <- ymd(data_nc)
  # Calcula a diferença das datas em dias
  dias <- difftime(data_ob, data_nc, units="days") 
  # Converte para inteiro
  dias <- strtoi(dias)
  # Calcula a idade em anos completos
  return(round(dias/365.25 - 0.5))
}

retorna_idade('2022-07-22', '2001-05-06')

# Identifica o tipo de variável
class(a)

subs<-substr(a ,start=0,stop=4)
class(subs)

# Sumariza o DF
glimpse(df_mort_inf)

# Cria a variável ano_nasc
df_mortes_inf = df_mortes_inf %>% mutate(ano_nasc = substr(DTNASC ,start=0,stop=4))

# Cria a variável ano_obito
df_mortes_inf = df_mortes_inf %>% mutate(ano_obito = substr(DTOBITO ,start=0,stop=4))

# Criar variável idade
df_mortes_inf = df_mortes_inf %>% mutate(idade = retorna_idade(DTOBITO, DTNASC))

# Filtrando por idade
df_mortes_inf <- filter(df_mortes_inf, idade<5)


# Criando o DF com os dados de óbitos a serem inseridos no DL

# Vetores que receberão os respectivos dados
vec_idade <- c()
vec_ano_ob <- c()
vec_ano_nc <- c()
vec_freq <- c()

# Adiciona dados aos vetores
for (idade_for in 0:4){
  for (ano_for in 2000:2021){
    # Informação para nascidos no mesmo ano (triângulo de baixo/direita)
    freq <- nrow(filter(df_mortes_inf, idade==idade_for, ano_obito==ano_for, ano_nasc==ano_for-idade_for))
    vec_idade <- append(vec_idade, idade_for)
    vec_ano_ob <- append(vec_ano_ob, ano_for)
    vec_ano_nc <- append(vec_ano_nc, ano_for-idade_for)
    vec_freq <- append(vec_freq, freq)
    # Informação para nascidos no ano anterior (triângulo de cima/esquerda)
    freq <- nrow(filter(df_mortes_inf, idade==idade_for, ano_obito==ano_for, ano_nasc==ano_for-idade_for-1))
    vec_idade <- append(vec_idade, idade_for)
    vec_ano_ob <- append(vec_ano_ob, ano_for)
    vec_ano_nc <- append(vec_ano_nc, ano_for-idade_for-1)
    vec_freq <- append(vec_freq, freq)
    
    }
}

# Cria o DF
obitos_DL <- data.frame(vec_idade, vec_ano_ob, vec_ano_nc, vec_freq)



# Diagrama de Lexis (grid)

# Função que adiciona caracteres em strings
insere_str <- function(ini, pos, insert) {       # Create own function
  gsub(paste0("^(.{", pos, "})(.*)$"),
       paste0("\\1", insert, "\\2"),
       ini)
}

#insere_str('2002', 4, '-01-05')

# Cria a base do DL
lexis <- lexis_grid(1995, 2022,0, 5) +
  xlab("Ano") + ylab("Idade")

# Pinta o espaço de vermelho
lexis <- lexis_year(lg = lexis, year=2000,22, fill = 'red', alpha = 0.4)

lexis <- lexis_cohort(lg = lexis, cohort = 2000, 17, fill = 'blue', alpha = 0.4)
lexis

# Adiciona "triângulos" amarelos para diferenciar os espaços
for (idade in 0:4){
  for (ano in 2000:2021){
    data1 <- insere_str(as.character(ano), 4, '-01-05')
    data2 <- insere_str(as.character(ano+1), 4, '-01-05')
    lexis <- lexis_polygon(lg = lexis, c(data1, data1, data2),
                           c(idade, idade+1, idade+1), fill = 'yellow', alpha = 0.4)
  }
}

# Adiciona a linha de nascimentos
lexis <- lexis_polygon(lg = lexis, c('2000-01-01', '2000-01-01', '2021-12-31',
                                     '2021-12-31'), c(0, 0.05, 0.05, 0),
                       fill = 'green', alpha = 0.8)


# Mostra o DL
lexis

# Exclui objeto da memória
#rm(df_mort_2021)




## QUESTÃO 1-B ##


# DL da questão 1.b
lexis <- lexis_grid(1995, 2022,0, 5) +
  xlab("Ano") + ylab("Idade")

# Pinta a área de interesse
lexis <- lexis_cohort(lg = lexis, cohort = 2000, 17, fill = 'blue', alpha = 0.4)
lexis

# Filtra por ano entre 2000 e 2016
df_ob_1b <- filter(obitos_DL, vec_ano_nc %in% (2000:2016) )
df_nc_1b <- filter(df_nasc, Ano.do.nascimento %in% (2000:2016) )

# Soma os óbitos e nascimentos
sum(df_ob_1b$vec_freq)
sum(df_nc_1b$Nascim_p.resid.mae)

# Calcula a razão
prob_1b <- 1 - sum(df_ob_1b$vec_freq) / sum(df_nc_1b$Nascim_p.resid.mae)
prob_1b # 0.9771792





## QUESTÃO 1-C ##


# DL da questão 1.c
lexis <- lexis_grid(1995, 2022,0, 5) +
  xlab("Ano") + ylab("Idade")

# Pinta a área de interesse
lexis <- lexis_polygon(lg = lexis, c('2001-01-01', '2001-01-01', '2020-12-31', '2020-12-31'),
                       c(0, 1, 1, 0), fill = 'blue', alpha = 0.4)
lexis <- lexis_polygon(lg = lexis, c('2000-01-10', '2000-12-31', '2000-12-31'),
                       c(0, 0, 1), fill = 'blue', alpha = 0.4)
lexis <- lexis_polygon(lg = lexis, c('2021-01-10', '2021-01-10', '2021-12-31'),
                       c(0, 1, 1), fill = 'blue', alpha = 0.4)
lexis


# Filtra por ano de nascimento e idade
df_ob_1c <- filter(obitos_DL, vec_ano_nc %in% (2000:2020), vec_idade == 0 )
df_nc_1c <- filter(df_nasc, Ano.do.nascimento %in% (2000:2020) )

# Soma os óbitos e nascimentos
sum(df_ob_1c$vec_freq)
sum(df_nc_1c$Nascim_p.resid.mae)

# Calcula a razão
prob_1c <- 1 - sum(df_ob_1c$vec_freq) / sum(df_nc_1c$Nascim_p.resid.mae)
prob_1c # 0.01936634
