
# Questão 2 - C

library(forcats)
library(ggplot2)
library(microdatasus)
library(dplyr)


# Banco de dados referentes à dados de 2021
View(df_nasc_2021)

df_nasc_2021 <- subset(df_nasc_2021, select = c(IDADEMAE, ESCMAE, PARTO))

df_nasc_2021$IDADEMAE <- as.integer(df_nasc_2021$IDADEMAE)

# Associação IDADEMAE e ESCMAE

# dados e informações filtradas pela variável ESCMAE
escm_nen <- filter(df_nasc_2021, ESCMAE=='Nenhuma')
mean(escm_nen$IDADEMAE) # 36
sd(escm_nen$IDADEMAE) # 6.237859
quantile(escm_nen$IDADEMAE, probs = c(0.25, 0.5, 0.75)) # 32  37  41 

escm_1_3 <- filter(df_nasc_2021, ESCMAE=='1 a 3 anos')
mean(escm_1_3$IDADEMAE) # 31.72754
sd(escm_1_3$IDADEMAE) # 7.064413
quantile(escm_1_3$IDADEMAE, probs = c(0.25, 0.5, 0.75)) # 26  33  37

escm_4_7 <- filter(df_nasc_2021, ESCMAE=='4 a 7 anos')
mean(escm_4_7$IDADEMAE) # 25.04701
sd(escm_4_7$IDADEMAE) # 7.120388
quantile(escm_4_7$IDADEMAE, probs = c(0.25, 0.5, 0.75)) # 19  24  30 

escm_8_11 <- filter(df_nasc_2021, ESCMAE=='8 a 11 anos')
mean(escm_8_11$IDADEMAE) # 26.1377
sd(escm_8_11$IDADEMAE) # 6.512233
quantile(escm_8_11$IDADEMAE, probs = c(0.25, 0.5, 0.75)) # 21  25  31 

escm_12 <- filter(df_nasc_2021, ESCMAE=='12 anos ou mais')
mean(escm_12$IDADEMAE) # 31.3021
sd(escm_12$IDADEMAE) # 5.484027
quantile(escm_12$IDADEMAE, probs = c(0.25, 0.5, 0.75)) # 27  31  35

# Calculo de R2
var_traco <- ( nrow(escm_nen)*var(escm_nen$IDADEMAE) + nrow(escm_1_3)*var(escm_1_3$IDADEMAE) + nrow(escm_4_7)*var(escm_4_7$IDADEMAE) +
                 nrow(escm_8_11)*var(escm_8_11$IDADEMAE) + nrow(escm_12)*var(escm_12$IDADEMAE) ) / 
  ( nrow(escm_nen) + nrow(escm_1_3) + nrow(escm_4_7) + nrow(escm_8_11) + nrow(escm_12) )

r2 <- 1 - var_traco / var(df_nasc_2021$IDADEMAE)
r2 # 0.1080924  apenas 10,81% da variabilidade de idademae pode ser explicada escmae


# Gráfico boxplot
graph_2C <- filter(df_nasc_2021, ESCMAE != 'NA')
# Ordena as barras
graph_2C$ESCMAE <- factor(graph_2C$ESCMAE,levels = c("Nenhuma", "1 a 3 anos",
                                        "4 a 7 anos", "8 a 11 anos", "12 anos ou mais"))
# Gera o gráfico
ggplot(graph_2C, aes(x=ESCMAE, y=IDADEMAE, fill=ESCMAE)) + 
  geom_boxplot() +
  theme(legend.position="none") +
  xlab("Escolaridade da Mãe") +
  ylab('Idade da Mãe')


# Associação PARTO e ESCMAE

# Dados para tabela de contingência (ignorou-se NAs)
df_nasc_2021 %>%
  group_by(ESCMAE, PARTO) %>%
  summarize(Freq=n())

# Teste qui-quadrado
?chisq.test

chisq.test(df_nasc_2021$ESCMAE, df_nasc_2021$PARTO, correct=T)
# Xcalc=2161.4 ; Xtab=9,488 ; p-valor tende a 0 ; rejeitamos H0



