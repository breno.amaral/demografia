
library(dplyr)
library(tidyverse)
library(ggplot2)

View(df_mortes_geral)
View(pop_2020)


# Tábua de vida - masculino

x <- c()
for (i in seq(0,80,5)){
  x <- append(x, i)
}
n <- c()
for (i in 1:16){
  n <- append(n, 5)
}
nkx <- c(1.9)
for (i in seq(5,75,5)){
  nkx <- append(nkx, 2.5)
}
nMx <- c()
nqx <- c()
lx <- c(100000)
ndx <- c()
npx <- c()
nLx <- c()
Tx <- c()
for (i in seq(1,16)){
  Tx <- append(Tx, NA)
}
ex <- c()

for (i in (1:16)){
  nMx <- append(nMx, nrow(filter(df_mt_masc, idade%in%((i-1)*5):((i*5)-1) )) /3 /pop_2020[1, i+1])
  nqx <- append(nqx, (5*nMx[i]) / (1 + (5-nkx[i])*nMx[i]) )
  ndx <- append(ndx, round(lx[i]*(nqx[i])))
  npx <- append(npx, 1-nqx[i])
  lx <- append(lx, lx[i]-ndx[i])
  nLx <- append(nLx, (lx[i]+lx[i+1])/2 *n[i])
}

n <- append(n, '+')
nkx <- append(nkx, mean(filter(df_mt_masc, idade>79)$idade)-80)
nMx <- append(nMx, nrow(filter(df_mt_masc, idade>79)) /3 /pop_2020[1, 18])
nqx <- append(nqx, 1)
ndx <- append(ndx, lx[17])
npx <- append(npx, 0)
nLx <- append(nLx, lx[17]*3/4 *5)

Tx[17] <- nLx[17]
for (i in seq(16, 1)){
  Tx[i] <- Tx[i+1]+nLx[i]
}

for (i in 1:17){
  ex <- append(ex, Tx[i]/lx[i])
}

TV_masc <- data.frame(x, n, nMx, nkx, nqx, lx, ndx, npx, nLx, Tx, ex)



# Tábua de vida - feminino

x <- c()
for (i in seq(0,80,5)){
  x <- append(x, i)
}
n <- c()
for (i in 1:16){
  n <- append(n, 5)
}
nkx <- c(1.9)
for (i in seq(5,75,5)){
  nkx <- append(nkx, 2.5)
}
nMx <- c()
nqx <- c()
lx <- c(100000)
ndx <- c()
npx <- c()
nLx <- c()
Tx <- c()
for (i in seq(1,16)){
  Tx <- append(Tx, NA)
}
ex <- c()

for (i in (1:16)){
  nMx <- append(nMx, nrow(filter(df_mt_fem, idade%in%((i-1)*5):((i*5)-1) )) /3 /pop_2020[2, i+1])
  nqx <- append(nqx, (5*nMx[i]) / (1 + (5-nkx[i])*nMx[i]) )
  ndx <- append(ndx, round(lx[i]*(nqx[i])))
  npx <- append(npx, 1-nqx[i])
  lx <- append(lx, lx[i]-ndx[i])
  nLx <- append(nLx, (lx[i]+lx[i+1])/2 *n[i])
}

n <- append(n, '+')
nkx <- append(nkx, mean(filter(df_mt_fem, idade>79)$idade)-80)
nMx <- append(nMx, nrow(filter(df_mt_fem, idade>79)) /3 /pop_2020[1, 18])
nqx <- append(nqx, 1)
ndx <- append(ndx, lx[17])
npx <- append(npx, 0)
nLx <- append(nLx, lx[17]*3/4 *5)

Tx[17] <- nLx[17]
for (i in seq(16, 1)){
  Tx[i] <- Tx[i+1]+nLx[i]
}

for (i in 1:17){
  ex <- append(ex, Tx[i]/lx[i])
}

TV_fem <- data.frame(x, n, nMx, nkx, nqx, lx, ndx, npx, nLx, Tx, ex)


# Gráficos

TV_masc$sexo <- 'Masculino'
TV_fem$sexo <- 'Feminino'
graph_3E <- rbind(TV_masc, TV_fem)

# Gráfico lx masc
ggplot(TV_masc, aes(x=x, y=lx)) +
  geom_line(stat = "identity", size=1, color='blue') +
  ylim(0,100000) +
  labs(title = "lx - masculino",
       y = "lx", x = "Idade")

# Gráfico lx fem
ggplot(TV_fem, aes(x=x, y=lx)) +
  geom_line(stat = "identity", size=1, color='red') +
  ylim(0,100000) +
  labs(title = "lx - feminino",
       y = "lx", x = "Idade")

# Gráfico lx
ggplot(graph_3E, aes(x=x, y=lx, group=sexo, color=sexo)) +
  geom_line(size=1) +
  ylim(0,100000) +
  labs(y = "lx", x = "Idade")

# Gráfico nqx masc
ggplot(TV_masc, aes(x=x, y=nqx)) +
  geom_line(stat = "identity", size=1, color='blue') +
  scale_y_continuous(trans = 'log10') +
  labs(title = "nqx - masculino",
       y = "nqx", x = "Idade")

# Gráfico nqx fem
ggplot(TV_fem, aes(x=x, y=nqx)) +
  geom_line(stat = "identity", size=1, color='red') +
  scale_y_continuous(trans = 'log10') +
  labs(title = "nqx - feminino",
       y = "nqx", x = "Idade")

# Gráfico nqx
ggplot(graph_3E, aes(x=x, y=nqx, group=sexo, color=sexo)) +
  geom_line(size=1) +
  scale_y_continuous(trans = 'log10') +
  labs(y = "nqx", x = "Idade")
