
library(microdatasus)
library(tidyverse)
library(lubridate)
library(dplyr)
library(ggplot2)

# Preparando DFs

df_mt_2015 <- fetch_datasus(year_start = 2015, year_end = 2015, uf = "SE", information_system = "SIM-DO")
df_mt_2015 <- process_sim(df_mt_2015)

df_mt_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DO21OPEN/DO21OPEN.dbf")
# Dados filtrados pela UF
df_mt_2021 <- df_mt_2021 %>%
  filter(CODMUNRES %in% df_mt_2015$CODMUNRES)
df_mt_2021 <- process_sim(df_mt_2021)

df_mt_2015 <- subset(df_mt_2015, select = c(SEXO, CAUSABAS))
df_mt_2015$ano <- 2015
df_mt_2015 <- na.omit(df_mt_2015)

df_mt_2021 <- subset(df_mt_2021, select = c(SEXO, CAUSABAS))
df_mt_2021$ano <- 2021
df_mt_2021 <- na.omit(df_mt_2021)

df_mt_causa <- rbind(df_mt_2015, df_mt_2021)

df_causa_masc <- filter(df_mt_causa, SEXO=="Masculino")
df_causa_fem <- filter(df_mt_causa, SEXO=="Feminino")


# Gráfico masc 2015

pop <- filter(df_mt_2015, SEXO=='Masculino')

# Monta tabela (ordenada) de frequência conforme CAUSABAS para o ano e sexo indicado
tab_freq <- rev(sort(table(pop$CAUSABAS)))

# Mostra os primeiros 7 valores (que entrarão na tabela) para observar as causas de morte
tab_freq[1:7]

# Vetor que contém as causas - montado manualmente a partir da execução da linha anterior
causa <- c('X954', 'I219', 'R99', 'J189', 'I64', ' I10', 'E149')

# Vetor das proporções
prop <- c()
for (i in 1:7){
  prop <- append(prop, tab_freq[i]/nrow(pop))
}
# Cria DF que será utilizado no gráfico
graph_masc_15 <- data.frame(causa, prop)

# Gráfico
# Ordena as barras
graph_masc_15$causa <- factor(graph_masc_15$causa,                                    # Change ordering manually
                 levels = causa)

# Mostra o gráfico
ggplot(graph_masc_15, aes(x=causa, y=prop)) +
  aes(x=causa, fill=causa) +
  geom_bar(stat = "identity") +
  labs(title = "Proporção de causa de morte pelo total - masculino - 2015",
       y = "Proporção", x = "Causa de morte")


# Gráfico fem 2015

pop <- filter(df_mt_2015, SEXO=='Feminino')

# Monta tabela (ordenada) de frequência conforme CAUSABAS para o ano e sexo indicado
tab_freq <- rev(sort(table(pop$CAUSABAS)))

# Mostra os primeiros 7 valores (que entrarão na tabela) para observar as causas de morte
tab_freq[1:7]

# Vetor que contém as causas - montado manualmente a partir da execução da linha anterior
causa <- c('I219', 'J189', 'I10', 'R99', 'E149', 'I64', 'C509')

# Vetor das proporções
prop <- c()
for (i in 1:7){
  prop <- append(prop, tab_freq[i]/nrow(pop))
}
# Cria DF que será utilizado no gráfico
graph_fem_15 <- data.frame(causa, prop)

# Gráfico
# Ordena as barras
graph_fem_15$causa <- factor(graph_fem_15$causa,
                              levels = causa)
# Mostra o gráfico
ggplot(graph_fem_15, aes(x=causa, y=prop)) +
  aes(x=causa, fill=causa) +
  geom_bar(stat = "identity") +
  #ylim(0, 0.15) +
  labs(title = "Proporção de causa de morte pelo total - feminino - 2015",
       y = "Proporção", x = "Causa de morte")


# Gráfico masc 2021

pop <- filter(df_mt_2021, SEXO=='Masculino')

# Monta tabela (ordenada) de frequência conforme CAUSABAS para o ano e sexo indicado
tab_freq <- rev(sort(table(pop$CAUSABAS)))

# Mostra os primeiros 7 valores (que entrarão na tabela) para observar as causas de morte
tab_freq[1:7]

# Vetor que contém as causas - montado manualmente a partir da execução da linha anterior
causa <- c('B342', 'I219', 'X954', 'R99', 'I10', 'C61', 'I64')

# Vetor das proporções
prop <- c()
for (i in 1:7){
  prop <- append(prop, tab_freq[i]/nrow(pop))
}
# Cria DF que será utilizado no gráfico
graph_masc_21 <- data.frame(causa, prop)

# Gráfico
# Ordena as barras
graph_masc_21$causa <- factor(graph_masc_21$causa,                                    # Change ordering manually
                              levels = causa)

# Mostra o gráfico
ggplot(graph_masc_21, aes(x=causa, y=prop)) +
  aes(x=causa, fill=causa) +
  geom_bar(stat = "identity") +
  labs(title = "Proporção de causa de morte pelo total - masculino - 2021",
       y = "Proporção", x = "Causa de morte")


# Gráfico fem 2021

pop <- filter(df_mt_2021, SEXO=='Feminino')

# Monta tabela (ordenada) de frequência conforme CAUSABAS para o ano e sexo indicado
tab_freq <- rev(sort(table(pop$CAUSABAS)))

# Mostra os primeiros 7 valores (que entrarão na tabela) para observar as causas de morte
tab_freq[1:7]

# Vetor que contém as causas - montado manualmente a partir da execução da linha anterior
causa <- c('B342', 'I219', 'R99', 'I10', 'E149', 'I64', 'C509')

# Vetor das proporções
prop <- c()
for (i in 1:7){
  prop <- append(prop, tab_freq[i]/nrow(pop))
}
# Cria DF que será utilizado no gráfico
graph_fem_21 <- data.frame(causa, prop)

# Gráfico
# Ordena as barras
graph_fem_21$causa <- factor(graph_fem_21$causa,
                             levels = causa)
# Mostra o gráfico
ggplot(graph_fem_21, aes(x=causa, y=prop)) +
  aes(x=causa, fill=causa) +
  geom_bar(stat = "identity") +
  #ylim(0, 0.15) +
  labs(title = "Proporção de causa de morte pelo total - feminino - 2021",
       y = "Proporção", x = "Causa de morte")
