
library(read.dbc)
library(lubridate)
library(dplyr)
library(ggplot2)

# Ajusta DFs de 2019, 2020 e 2021
df_mortes_2019 <- read.dbc("C:/Users/breno/Downloads/DOSE2019.dbc")
df_mortes_2019 <- process_sim(df_mortes_2019)
df_mortes_2019 <- subset(df_mortes_2019, select = c(DTOBITO, DTNASC, SEXO, CAUSABAS))
df_mortes_2019 <- df_mortes_2019 %>% mutate(idade = retorna_idade(DTOBITO, DTNASC))

df_mortes_2020 <- process_sim(df_mortes_2020)
df_mortes_2020 <- subset(df_mortes_2020, select = c(DTOBITO, DTNASC, SEXO, CAUSABAS))
df_mortes_2020 <- df_mortes_2020 %>% mutate(idade = retorna_idade(DTOBITO, DTNASC))

df_mortes_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DO21OPEN/DO21OPEN.dbf")
# Dados filtrados pela UF
df_mortes_2021 <- df_mortes_2021 %>%
  filter(CODMUNRES %in% df_mortes_2020$CODMUNRES)
df_mortes_2021 <- process_sim(df_mortes_2021)
df_mortes_2021 <- subset(df_mortes_2021, select = c(DTOBITO, DTNASC, SEXO, CAUSABAS))
df_mortes_2021 <- df_mortes_2021 %>% mutate(idade = retorna_idade(DTOBITO, DTNASC))


# QUESTÃO 3.A.1 - TBM

# População de 2020
sum(pop_2020$Total) # 2318822

# Média de mortes - 2019, 2020 e 2021
mortes_med <- (nrow(df_mortes_2019) + nrow(df_mortes_2020)+ nrow(df_mortes_2021))/3
mortes_med # 15428.67

 # Cálculo da taxa
tbm <- mortes_med/sum(pop_2020$Total) * 1000
tbm # 6.653666


# QUESTÃO 3.A.2 - TEM por sexo e idade

df_mortes_2019$ano <- 2019
df_mortes_2020$ano <- 2020
df_mortes_2021$ano <- 2021

df_mortes_geral <- rbind(df_mortes_2019, df_mortes_2020, df_mortes_2021)

# TEM Masculino

# Gera DF com TEM masc para todos os intervalos

inter <- c(NA)
tem <- c(NA)
tem_m <- data.frame(inter, tem)

for (i in seq(0,15)){
  mostra <- nrow(filter(df_mortes_geral, SEXO=='Masculino', idade %in% ( (i*5) : ((i*5)+4) ))) /
    3 / pop_2020[1, i+2] * 1000
  print(paste(i*5,'-', i*5+4,':', mostra))
  tem_m[nrow(tem_m) + 1,] <- c(paste(i*5,'-', i*5+4), mostra)
}

tem_m[nrow(tem_m) + 1,] <- c(paste('80 ou mais'), nrow(filter(df_mortes_geral, SEXO=='Masculino',
                                                              idade > 79)) / 3 / pop_2020[1, 18] * 1000)

tem_m <- na.omit(tem_m)

tem_m$tem <- as.numeric(tem_m$tem)

# TEM Fem

# Gera DF com TEM fem para todos os intervalos

inter <- c(NA)
tem <- c(NA)
tem_f <- data.frame(inter, tem)

for (i in seq(0,15)){
  mostra <- nrow(filter(df_mortes_geral, SEXO=='Feminino', idade %in% ( (i*5) : ((i*5)+4) ))) /
    3 / pop_2020[2, i+2] * 1000
  print(paste(i*5,'-', i*5+4,':', mostra))
  tem_f[nrow(tem_f) + 1,] <- c(paste(i*5,'-', i*5+4), mostra)
}

tem_f[nrow(tem_f) + 1,] <- c(paste('80 ou mais'), nrow(filter(df_mortes_geral, SEXO=='Feminino',
                                                              idade > 79)) / 3 / pop_2020[2, 18] * 1000)

tem_f <- na.omit(tem_f)

tem_f$tem <- as.numeric(tem_f$tem)


# Gráficos

tem_m$sexo <- 'Masculino'
tem_f$sexo <- 'Feminino'
tem_m$num <- c(0:16)
tem_f$num <- c(0:16)
tem_geral <- rbind(tem_m, tem_f)

# Mostra as variáveis do df
glimpse(tem_geral)

# Gráfico em escala padrão
tem_geral %>%
  ggplot( aes(x=num, y=tem, group=sexo, color=sexo)) +
  geom_line(size=1) +
  ylab("Taxa Específica de Mortalidade") 

# Gráfico em escala log
tem_geral %>%
  ggplot( aes(x=num, y=tem, group=sexo, color=sexo)) +
  scale_y_continuous(trans = 'log10') +
  geom_line(size=1) +
  ylab("Taxa Específica de Mortalidade") 
  
# Gráfico para usar a escala do eixo X
tem_geral %>%
  ggplot( aes(x=inter, y=tem, group=sexo, color=sexo)) +
  geom_line(size=1) +
  xlab("Intervalo por idade") 



# QUESTÃO 3.B - TMI e outras

# Número de nascimentos em 2020
num_nasc_2020 <- 31784

# Mortes na infância 2019-2021
df_MI <- filter(df_mortes_geral, idade < 5)

# TM na infância calculada
tmi <- nrow(df_MI)/3/num_nasc_2020*1000
tmi # 17.7238

# TM infantil
# Adiciona variável dias de vida
df_MI = df_MI %>% mutate(dias = difftime(ymd(DTOBITO), ymd(DTNASC), units = 'days'))

glimpse(df_MI)

tm_infantil <- nrow(filter(df_MI, dias<366))/3/num_nasc_2020*1000
tm_infantil # 15.65777

# taxa de mortalidade neonatal

tmn <- nrow(filter(df_MI, dias<28))/3/num_nasc_2020*1000
tmn # 11.27402

# taxa de mortalidade neonatal precoce

tmnp <- nrow(filter(df_MI, dias<7))/3/num_nasc_2020*1000
tmnp # 8.106804

# taxa de mortalidade neonatal tardia 

tmnt <- nrow(filter(df_MI, dias %in% 7:27))/3/num_nasc_2020*1000
tmnt # 3.167212

# taxa de mortalidade posneonatal

tmpn <- nrow(filter(df_MI, dias %in% 28:365))/3/num_nasc_2020*1000
tmpn # 4.383757

# taxa de mortalidade perinatal 

# número de óbitos fetais - obtidos no datasus-tabnet
# 2019: 351    2020:385    total: 736

# Óbitos de 2021
ob_fet_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DO21OPEN/DO21OPEN.dbf")

# Óbitos fetais de 2021
ob_fet_2021 <- nrow(filter(ob_fet_2021, TIPOBITO=='Fetal'))
ob_fet_2021 # 376

# Cálculo da taxa
num_ob_fetais <- (736+ob_fet_2021) /3

tmp <- ( nrow(filter(df_MI, dias<7))/3 + num_ob_fetais ) /
  ( num_nasc_2020 + num_ob_fetais) *1000
tmp # 19.54097
