library(read.dbc)
library(microdatasus)
library(tidyverse)
library(lubridate)
library(dplyr)
library(pacman)
library(LexisPlotR)
library(ggplot2)

# for com passo
for (i in seq(1, 10, 2)){
  print(i)
}

# Números de nascimentos de 2019 a 2021
Q2_df_nasc <- slice(df_nasc, 20:22)
Q2_num_nasc <- mean(Q2_df_nasc$Nascim_p.resid.mae)

# QUESTÃO 2.A.1 - TBN

# Vetor com as estimativas de população
vec_pop <- c(2298696, 2318822, 2338474)
# Média da população
pop_bruta <- mean(vec_pop)

tbn <- (Q2_num_nasc/pop_bruta) * 1000
tbn # 13.75591


# QUESTÃO 2.A.2 - TFG nFX

# Cria DF da pop
pop_2019 <- read.csv("C:/Users/breno/Downloads/pop_2019.csv", sep=';')
pop_2019 = pop_2019 %>% mutate(ano = 2019)
pop_2020 <- read.csv("C:/Users/breno/Downloads/pop_2020.csv", sep=';')
pop_2020 = pop_2020 %>% mutate(ano = 2020)
pop_2021 <- read.csv("C:/Users/breno/Downloads/pop_2021.csv", sep=';')
pop_2021 = pop_2021 %>% mutate(ano = 2021)

pop_geral <- rbind(pop_2019, pop_2020, pop_2021)

pop_geral <- filter(pop_geral, Sexo == "Feminino")
# Calcula a média dos anos
tab_pop <- colMeans(pop_geral[2:19])

tab_pop
tab_pop[7]

# Importando dados de 2019 (de 2020 e 2021 já foram importados na questão 1)
df_nasc_2019 <- read.dbc("C:/Users/breno/Downloads/arqdn19/DNSE2019.dbc")

# Deixando apenas a variável idade mãe
Q2_nasc_2019 <- subset(df_nasc_2019, select = IDADEMAE)
Q2_nasc_2020 <- subset(df_nasc_2020, select = IDADEMAE)
Q2_nasc_2021 <- subset(df_nasc_2021, select = IDADEMAE)

Q2_nasc_geral <- rbind(Q2_nasc_2019, Q2_nasc_2020, Q2_nasc_2021)

# Adicionando variável intervalo_idade
# Cria coluna com NAs
Q2_nasc_geral$int_idade <- rep(NA, nrow(Q2_nasc_geral))
# Adiciona valores conforme condição
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 50] <- '45_49'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 45] <- '40_44'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 40] <- '35_39'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 35] <- '30_34'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 30] <- '25_29'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 25] <- '20_24'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 20] <- '15_19'
Q2_nasc_geral$int_idade[strtoi(Q2_nasc_geral$IDADEMAE) < 15] <- NA

Q2_nasc_geral


nrow(filter(Q2_nasc_geral, int_idade == '15_19'))

# TFG
pop_tfg <- tab_pop[4:10]
tfg <- (Q2_num_nasc/sum(pop_tfg)) * 1000
tfg # 47.81013

# TEF 15-19
tef_15_19 <- nrow(filter(Q2_nasc_geral, int_idade == '15_19')) /3 / tab_pop[4] * 1000
tef_15_19 # 54.22292

# TEF 20_24
tef_20_24 <- nrow(filter(Q2_nasc_geral, int_idade == '20_24')) /3 / tab_pop[5] * 1000
tef_20_24 # 76.29323

# TEF 25_29
tef_25_29 <- nrow(filter(Q2_nasc_geral, int_idade == '25_29')) /3 / tab_pop[6] * 1000
tef_25_29 # 72.16303

# TEF 30_34
tef_30_34 <- nrow(filter(Q2_nasc_geral, int_idade == '30_34')) /3 / tab_pop[7] * 1000
tef_30_34 # 60.71903

# TEF 35_39
tef_35_39 <- nrow(filter(Q2_nasc_geral, int_idade == '35_39')) /3 / tab_pop[8] * 1000
tef_35_39 # 39.68821

# TEF 40_44
tef_40_44 <- nrow(filter(Q2_nasc_geral, int_idade == '40_44')) /3 / tab_pop[9] * 1000
tef_40_44 # 11.83739

# TEF 45_49
tef_45_49 <- nrow(filter(Q2_nasc_geral, int_idade == '45_49')) /3 / tab_pop[10] * 1000
tef_45_49 # 0.8424312


# Gráfico

# Cria df do gráfico
graph_2A <- data.frame(
  Idades = c('15-19', '20-24', '25-29', '30-34', '35-39', '40-44', '45-49') ,  
  value = c(tef_15_19, tef_20_24, tef_25_29, tef_30_34, tef_35_39, tef_40_44, tef_45_49)
)

# Barplot
ggplot(graph_2A, aes(x=Idades, y=value)) +
  aes(x=Idades, fill=Idades) +
  geom_bar(stat = "identity") +
  labs(title = "Taxas Específicas de Fecundidade",
       y = "TEF", x = "Intervalo da idade da mãe")


# QUESTÃO 2.A.3 - TFT

tft <- (tef_15_19 + tef_20_24 + tef_25_29 + tef_30_34 + tef_35_39 + tef_40_44 + tef_45_49)* 5 / 1000
tft # 1.578831 

# QUESTÃO 2.A.4 - TEFF

# Recriando os bancos de dados
df_nasc_2019 <- process_sim(df_nasc_2019)
df_nasc_2020 <- process_sim(df_nasc_2020)
df_nasc_2021 <- process_sim(df_nasc_2021)
df_nasc_2021 <- df_nasc_2021[2:70]

# Deixando apenas variáveis de interesse do total
df_2d_2019 <- subset(df_nasc_2019, select = c(IDADEMAE, SEXO))
df_2d_2020 <- subset(df_nasc_2020, select = c(IDADEMAE, SEXO))
df_2d_2021 <- subset(df_nasc_2021, select = c(IDADEMAE, SEXO))

df_2d_geral <- rbind(df_2d_2019, df_2d_2020, df_2d_2021)

df_2d_geral <- filter(df_2d_geral, SEXO == 'Feminino')

# Adicionando variável intervalo_idade
# Cria coluna com NAs
df_2d_geral$int_idade <- rep(NA, nrow(df_2d_geral))
# Adiciona valores conforme condição
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 50] <- '45_49'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 45] <- '40_44'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 40] <- '35_39'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 35] <- '30_34'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 30] <- '25_29'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 25] <- '20_24'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 20] <- '15_19'
df_2d_geral$int_idade[strtoi(df_2d_geral$IDADEMAE) < 15] <- NA


# TEFF = TEFN
# TEFN 15-19
tefn_15_19 <- nrow(filter(df_2d_geral, int_idade == '15_19')) /3 / tab_pop[4] * 1000
tefn_15_19 # 26.23528 

# TEFN 20_24
tefn_20_24 <- nrow(filter(df_2d_geral, int_idade == '20_24')) /3 / tab_pop[5] * 1000
tefn_20_24 # 37.43225

# TEFN 25_29
tefn_25_29 <- nrow(filter(df_2d_geral, int_idade == '25_29')) /3 / tab_pop[6] * 1000
tefn_25_29 # 34.96081

# TEFN 30_34
tefn_30_34 <- nrow(filter(df_2d_geral, int_idade == '30_34')) /3 / tab_pop[7] * 1000
tefn_30_34 # 29.68833

# TEFN 35_39
tefn_35_39 <- nrow(filter(df_2d_geral, int_idade == '35_39')) /3 / tab_pop[8] * 1000
tefn_35_39 # 19.30933

# TEFN 40_44
tefn_40_44 <- nrow(filter(df_2d_geral, int_idade == '40_44')) /3 / tab_pop[9] * 1000
tefn_40_44 # 5.490996

# TEFN 45_49
tefn_45_49 <- nrow(filter(df_2d_geral, int_idade == '45_49')) /3 / tab_pop[10] * 1000
tefn_45_49 # 0.4532878


# Gráfico

# Cria df do gráfico
graph_2A.4 <- data.frame(
  Idades = c('15-19', '20-24', '25-29', '30-34', '35-39', '40-44', '45-49') ,  
  value = c(tefn_15_19, tefn_20_24, tefn_25_29, tefn_30_34, tefn_35_39, tefn_40_44, tefn_45_49)
)

# Barplot
ggplot(graph_2A.4, aes(x=Idades, y=value)) +
  aes(x=Idades, fill=Idades) +
  geom_bar(stat = "identity") +
  labs(title = "Taxas Específicas de Fecundidade Feminina",
       y = "TEF - Feminina", x = "Intervalo da idade da mãe")


# Questão 2.A.5 - TBR
tbr <- (tefn_15_19 + tefn_20_24 + tefn_25_29 + tefn_30_34 + tefn_35_39 + tefn_40_44 + tefn_45_49) * 
  5 / 1000
tbr # 0.7678514 


# Questão 2.A.6 - TLR

# Cria DF de mortes em 2021 para filtra-la por feminino
df_mortes_2021 <- foreign::read.dbf("C:/Users/breno/Downloads/DO21OPEN/DO21OPEN.dbf")
# Baixando o de 2020 para comparar e obter os dados de 2021 pela UF de interesse
df_mortes_2020 <- read.dbc("C:/Users/breno/Downloads/arqdo/DOSE2020.dbc")

# Dados filtrados pela UF
df_mortes_2021 <- df_mortes_2021 %>%
  filter(CODMUNRES %in% df_mortes_2020$CODMUNRES)
# Trata os dados
df_mortes_2021 <- process_sim(df_mortes_2021)

# Deixando apenas variáveis de interesse de 2021
df_mortes_2021 <- subset(df_mortes_2021, select = c(DTOBITO, DTNASC, SEXO))

# Retira NAs
df_mortes_2021 <- na.omit(df_mortes_2021)
# Filtra por feminino
df_mortes_fem_2021 <- filter(df_mortes_2021, SEXO == 'Feminino')
# Cria variável idade
df_mortes_fem_2021 = df_mortes_fem_2021 %>% mutate(idade = retorna_idade(DTOBITO, DTNASC))

# Adicionando variável intervalo_idade
# Cria coluna com NAs
df_mortes_fem_2021$int_idade <- rep(NA, nrow(df_mortes_fem_2021))
# Adiciona valores conforme condição
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 50] <- '45_49'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 45] <- '40_44'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 40] <- '35_39'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 35] <- '30_34'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 30] <- '25_29'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 25] <- '20_24'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 20] <- '15_19'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 15] <- '10_14'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 10] <- '05_09'
df_mortes_fem_2021$int_idade[strtoi(df_mortes_fem_2021$idade) < 5] <- '00_04'

# Conta as frequências por intervalo de idade
nrow(filter(df_mortes_fem_2021, int_idade == '00_04')) # 224
nrow(filter(df_mortes_fem_2021, int_idade == '05_09')) # 19
nrow(filter(df_mortes_fem_2021, int_idade == '10_14')) # 16
nrow(filter(df_mortes_fem_2021, int_idade == '15_19')) # 38
nrow(filter(df_mortes_fem_2021, int_idade == '20_24')) # 51
nrow(filter(df_mortes_fem_2021, int_idade == '25_29')) # 74
nrow(filter(df_mortes_fem_2021, int_idade == '30_34')) # 91
nrow(filter(df_mortes_fem_2021, int_idade == '35_39')) # 150
nrow(filter(df_mortes_fem_2021, int_idade == '40_44')) # 208
nrow(filter(df_mortes_fem_2021, int_idade == '45_49')) # 296

# Importa df de mortes femininas de 2019 e 2020
df_mortes_fem_19_20 <- read.csv("C:/Users/breno/Downloads/mort_fem_19_20.csv", sep=';')

# criando vetor que contém a média de mortes feminina
med_mort <- c()

Mx <- ( df_mortes_fem_19_20$X0.a.6.dias[3] + df_mortes_fem_19_20$X7.a.27.dias[3] +
  df_mortes_fem_19_20$X28.a.364.dias[3] + df_mortes_fem_19_20$X1.a.4.anos[3] +
    nrow(filter(df_mortes_fem_2021, int_idade == '00_04')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X5.a.9.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '05_09')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X10.a.14.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '10_14')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X15.a.19.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '15_19')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X20.a.24.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '20_24')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X25.a.29.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '25_29')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X30.a.34.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '30_34')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X35.a.39.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '35_39')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X40.a.44.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '40_44')) )/3
med_mort <- append(med_mort, Mx)

Mx <- ( df_mortes_fem_19_20$X45.a.49.anos[3] +
          nrow(filter(df_mortes_fem_2021, int_idade == '45_49')) )/3
med_mort <- append(med_mort, Mx)

# Adiciona valores à coluna nMx
nMx <- c()
for (i in (1:10)){
  nMx <- append(nMx, med_mort[i]/tab_pop[i])
}
nMx

# Cria os vetores e adiciona os valores correspondentes
nqx <- c()
lx <- c(100000)
ndx <- c()
for (i in (1:10)){
  k <- ifelse(i==1, 1.9, 2.5)
  #print(k)
  nqx <- append(nqx, (5*nMx[i]) / (1 + (5-k)*nMx[i]) )
  #print(nqx)
  ndx <- append(ndx, round(lx[i]*(nqx[i])))
  #print(ndx)
  lx <- append(lx, lx[i]-ndx[i])
  #print(lx)
  }

ndx = append(ndx, NA)
nqx = append(nqx, NA)
nMx = append(nMx, NA)

idade_ini <- c(seq(0, 50, 5))
idade_ini

# Cria tabela com dados para tábua de vida
tv_2a <- data.frame(idade_ini, nMx, nqx, lx, ndx)

# nLx = 15L50
nLx <- (lx[4]+lx[11])/2 /lx[1]
nLx # 0.96267

# TLR (Resposta!)
tlr <- tbr * nLx
tlr # 0.7391875 

lx
