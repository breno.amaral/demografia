library(dplyr)
library(ggplot2)

# Prepara o DF de população
populacao_mundial <- read.csv("C:/Users/breno/Downloads/populacao_mundial.csv", sep=";")
# Renomeia nomes equivocados de colunas
populacao_mundial <- rename(populacao_mundial, 'X5.9' = 'X05.set')
populacao_mundial <- rename(populacao_mundial, 'X10.14' = 'out.14')
populacao_mundial <- rename(populacao_mundial, 'Country' = 'Region..subregion..country.or.area..')
# Filtra pela população de interesse no ano desejado
lis_paises <- c('WORLD', 'Croatia', 'Ecuador')
pop_interesse <- filter(populacao_mundial, Year==2021, Country %in% lis_paises)
# Retira variáveis inúteis
drop <- c('Index', 'Variant', 'Notes', 'Location.code',
          'ISO3.Alpha.code', 'ISO2.Alpha.code', 'SDMX.code..',
          'Type', 'Parent.code', 'Year' )
pop_interesse = pop_interesse[,!(names(pop_interesse) %in% drop)]

# Prepara o DF de mortalidade
mortalidade_mundial <- read.csv("C:/Users/breno/Downloads/mortalidade_mundial.csv", sep=";")
mortalidade_mundial <- rename(mortalidade_mundial, 'Country' = 'Region..subregion..country.or.area..')
mort_interesse <- filter(mortalidade_mundial, Year==2021, Country %in% lis_paises)
mort_interesse = mort_interesse[,!(names(mort_interesse) %in% drop)]

# Prepara o DF de tábua de vida
tv_mundial <- read.csv2("C:/Users/breno/Downloads/tv_mundial.csv")
tv_mundial <- rename(tv_mundial, 'Country' = 'Region..subregion..country.or.area..')
tv_interesse <- filter(tv_mundial, Year==2021, Country %in% lis_paises)
tv_interesse = tv_interesse[,!(names(tv_interesse) %in% drop)]

# Função que tira espaços
tira_esp <- function(texto){
  texto <- toString(texto)
  return (gsub(' ', '', texto))
}

# Adequando pop_interesse
# Aplicando a função para as colunas desejadas
for (i in 1:3){
  for (j in 2:22){
    pop_interesse[i,j] <- tira_esp(pop_interesse[i,j])
  }
}
# Transformando em inteiro
pop_interesse[,2:22] <- lapply(pop_interesse[,2:22], as.integer)

# Adequando mort_interesse
# Aplicando a função para as colunas desejadas
for (i in 1:3){
  for (j in 2:102){
    mort_interesse[i,j] <- tira_esp(mort_interesse[i,j])
  }
}
# Transformando em inteiro
mort_interesse[,2:102] <- lapply(mort_interesse[,2:102], as.integer)

# Adequando tv_interesse
# Aplicando a função para as colunas desejadas
for (i in 1:66){
  for (j in 2:13){
    tv_interesse[i,j] <- tira_esp(tv_interesse[i,j])
    tv_interesse[i,j] <- gsub(',', '.', tv_interesse[i,j])
  }
}
# Transformando em racional
tv_interesse[,2:13] <- lapply(tv_interesse[,2:13], as.numeric)

# Sumários
glimpse(pop_interesse)
glimpse(mort_interesse)
glimpse(tv_interesse)

# Populações totais em milhares
pop_total_mundo <- sum(pop_interesse[1,2:22])
pop_total_croacia <- sum(pop_interesse[2,2:22])
pop_total_equador <- sum(pop_interesse[3,2:22])


col_ncx <- c()
for (i in seq(0,95,5)){
  col_ncx <- append(col_ncx, tira_esp(paste(i,'C',i+4)))
}
col_ncx <- append(col_ncx, paste('100C+'))

mundo <- c()
croacia <- c()
equador <- c()
for (i in 1:21){
  mundo <- append(mundo, pop_interesse[1, i+1]/pop_total_mundo)
  croacia <- append(croacia, pop_interesse[2, i+1]/pop_total_croacia)
  equador <- append(equador, pop_interesse[3, i+1]/pop_total_equador)
}
tab_ncx <- data.frame(col_ncx, mundo, croacia, equador)

# Cálculo TBM Mundo
tbm_mun <- c((tv_interesse$Central.death.rate.m.x.n.[1]*0.2 +
               tv_interesse$Central.death.rate.m.x.n.[2]*0.8)*tab_ncx[1,2]*1000)
for (i in 2:21){
  tbm_mun <- append(tbm_mun, tv_interesse$Central.death.rate.m.x.n.[i+1]*tab_ncx[i,2]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+1], tab_ncx[i,2]))
}
tbm_mun
# TBM Mundo
sum(tbm_mun) #8.792858


# Cálculo TBM Croácia
tbm_cro <- c((tv_interesse$Central.death.rate.m.x.n.[23]*0.2 +
                tv_interesse$Central.death.rate.m.x.n.[24]*0.8)*tab_ncx[1,3]*1000)
for (i in 2:21){
  tbm_cro <- append(tbm_cro, tv_interesse$Central.death.rate.m.x.n.[i+23]*tab_ncx[i,3]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+23], tab_ncx[i,3]))
}
tbm_cro
# TBM Croácia
sum(tbm_cro) #15.28174

# Cálculo TBM Equador
tbm_equ <- c((tv_interesse$Central.death.rate.m.x.n.[45]*0.2 +
                tv_interesse$Central.death.rate.m.x.n.[46]*0.8)*tab_ncx[1,4]*1000)
for (i in 2:21){
  tbm_equ <- append(tbm_equ, tv_interesse$Central.death.rate.m.x.n.[i+45]*tab_ncx[i,4]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+45], tab_ncx[i,4]))
}
tbm_equ
# TBM Equador
sum(tbm_equ) #6.751151


# Padronização Direta

# Cálculo TBM - padronização direta - Croácia
pad_dir_cro <- c((tv_interesse$Central.death.rate.m.x.n.[23]*0.2 +
                tv_interesse$Central.death.rate.m.x.n.[24]*0.8)*tab_ncx[1,2]*1000)
for (i in 2:21){
  pad_dir_cro <- append(pad_dir_cro, tv_interesse$Central.death.rate.m.x.n.[i+23]*tab_ncx[i,2]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+23], tab_ncx[i,2]))
}
pad_dir_cro
# TBM - padronização direta - Croácia
sum(pad_dir_cro) #6.75205

# Cálculo TBM - padronização direta - Equador
pad_dir_equ <- c((tv_interesse$Central.death.rate.m.x.n.[45]*0.2 +
                tv_interesse$Central.death.rate.m.x.n.[46]*0.8)*tab_ncx[1,2]*1000)
for (i in 2:21){
  pad_dir_equ <- append(pad_dir_equ, tv_interesse$Central.death.rate.m.x.n.[i+45]*tab_ncx[i,2]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+45], tab_ncx[i,2]))
}
pad_dir_equ
# TBM - padronização direta - Equador
sum(pad_dir_equ) #8.107804


# Padronização Indireta

# Cálculo TBM - padronização indireta - Croácia
pad_ind_cro <- c((tv_interesse$Central.death.rate.m.x.n.[1]*0.2 +
                tv_interesse$Central.death.rate.m.x.n.[2]*0.8)*tab_ncx[1,3]*1000)
for (i in 2:21){
  pad_ind_cro <- append(pad_ind_cro, tv_interesse$Central.death.rate.m.x.n.[i+1]*tab_ncx[i,3]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+1], tab_ncx[i,3]))
}
pad_ind_cro
# TBM - padronização indireta - Croácia
sum(pad_ind_cro) #16.98134

# Cálculo TBM - padronização indireta - Equador
pad_ind_equ <- c((tv_interesse$Central.death.rate.m.x.n.[1]*0.2 +
                    tv_interesse$Central.death.rate.m.x.n.[2]*0.8)*tab_ncx[1,4]*1000)
for (i in 2:21){
  pad_ind_equ <- append(pad_ind_cro, tv_interesse$Central.death.rate.m.x.n.[i+1]*tab_ncx[i,4]*1000)  
  print(paste(tv_interesse$Central.death.rate.m.x.n.[i+1], tab_ncx[i,4]))
}
pad_ind_equ
# TBM - padronização indireta - Equador
sum(pad_ind_equ) #17.00549



# Gráfico nMx escala log10
tv_interesse %>%
  ggplot( aes(x=Age..x., y=Central.death.rate.m.x.n., group=Country, color=Country)) +
  scale_y_continuous(trans = 'log10') +
  geom_line(size=1) +
  ylab("nMx") +
  xlab('Idade') +
  ggtitle('Decomposição da Mortalidade')


# Cria variável para entrar na TV
nCx <- c(tab_ncx[2,2]+(tab_ncx[1,2]-tab_ncx[2,2])/2.5*4,
         tab_ncx[2,2]+(tab_ncx[1,2]-tab_ncx[2,2])/2.5*2)
for (i in 2:21){
  nCx <- append(nCx, tab_ncx[i,2])
}
nCx <- append(nCx, tab_ncx[2,3]+(tab_ncx[1,3]-tab_ncx[2,3])/2.5*4)
nCx <- append(nCx, tab_ncx[2,3]+(tab_ncx[1,3]-tab_ncx[2,3])/2.5*2)
for (i in 2:21){
  nCx <- append(nCx, tab_ncx[i,3])
}
nCx <- append(nCx, tab_ncx[2,4]+(tab_ncx[1,4]-tab_ncx[2,4])/2.5*4)
nCx <- append(nCx, tab_ncx[2,4]+(tab_ncx[1,4]-tab_ncx[2,4])/2.5*2)
for (i in 2:21){
  nCx <- append(nCx, tab_ncx[i,4])
}
nCx

tv_interesse$nCx <- nCx


# Gráfico nCx escala padrão
tv_interesse %>%
  ggplot( aes(x=Age..x., y=nCx, group=Country, color=Country)) +
  geom_line(size=1) +
  ylab("nCx") +
  xlab('Idade') +
  ggtitle('Decomposição da População')
